import uuid
import datetime

import botocore
import pytz
from flask import request, jsonify

from repo import postRepo, commentRepo


def save_post():
    post_id = str(uuid.uuid4())
    title = request.json.get('title')
    body = request.json.get('body')
    time = current_time()

    if not post_id or not title:
        return jsonify({'error': 'Please provide title'}), 400

    try:
        postRepo.put_item(body, time, post_id, title, time)
    except botocore.errorfactory.ConditionalCheckFailedException as e:
        return jsonify({'error': 'A post with this title already exists'}), 409

    return jsonify({
        'id': post_id,
        'title': title,
        'body': body,
        'created_at': time,
        'updated_at': time
    }), 201


def save_comment(post_id):
    comment_id = str(uuid.uuid4())
    body = request.json.get('body')
    time = current_time()

    resp = postRepo.get_post(post_id)

    item = resp.get('Item')
    if not item or 'error' in item:
        return jsonify({'error': 'Post ID does not exist'}), 404
    elif not body:
        return jsonify({'error': 'Please provide comment body'}), 400

    commentRepo.put_comment(body, comment_id, time, post_id, time)

    return jsonify({
        'id': comment_id,
        'post_id': post_id,
        'body': body,
        'created_at': time,
        'updated_at': time
    }), 201


def update_post(post_id):
    resp = postRepo.get_post(post_id)
    item = resp.get('Item')
    if not item or 'error' in item:
        return jsonify({'error': 'Post ID does not exist'}), 404
    title = request.json.get('title') if request.json.get('title') is not None else item.get('title')
    body = request.json.get('body') if request.json.get('body') is not None else item.get('body')

    if not post_id or not title:
        return jsonify({'error': 'Please provide title'}), 400

    resp = postRepo.update_post(body, post_id, title, current_time()).get('Item')

    comments = commentRepo.get_comments_by_post_id(post_id).get('Items')
    resp['comments'] = comments
    return resp, 200


def update_comment(post_id, comment_id):
    comment = commentRepo.get_comment(comment_id).get('Item')

    if not comment or 'error' in comment:
        return jsonify({'error': 'Comment ID does not exist'}), 404

    if request.json.get('body') is None or post_id != comment.get('post_id'):
        return jsonify({'error': 'Bad request'}), 400

    body = request.json.get('body')

    commentRepo.update_comment(body, comment_id, current_time())

    return commentRepo.get_comment(comment_id).get('Item'), 200


def get_post(post_id):
    resp = postRepo.get_post(post_id).get('Item')

    if not resp:
        return jsonify({'error': 'Post does not exist'}), 404

    resp['comments'] = commentRepo.get_comments_by_post_id(post_id).get('Items')

    return resp, 200


def delete_post(post_id):

    comments = commentRepo.get_comments_by_post_id(post_id).get('Items')

    for c in comments:
        commentRepo.delete_comment(c.get('id'))

    resp = postRepo.delete_post(post_id)

    if not resp:
        return jsonify({'error': 'Post does not exist'}), 404

    return jsonify({'status': 'Post {} has been deleted'.format(post_id)}), 200


def get_posts():
    return jsonify(postRepo.scan_posts().get('Items')), 200


def get_comment(post_id, comment_id):
    resp = commentRepo.get_comment(comment_id).get('Item')

    if not resp:
        return jsonify({'error': 'Comment does not exist'}), 404
    if resp.get('post_id') is not post_id:
        jsonify({'error': 'Comment does not belong to provided post ID'}), 400

    return resp, 200


def delete_comment(post_id, comment_id):
    resp = commentRepo.delete_comment(comment_id)

    if not resp:
        return jsonify({'error': 'Comment does not exist'}), 404
    if resp.get('post_id') is not post_id:
        jsonify({'error': 'Comment does not belong to provided post ID'}), 400

    return jsonify({'status': 'Comment {} has been deleted'.format(comment_id)}), 200


def get_comments(post_id):
    return jsonify(commentRepo.get_comments_by_post_id(post_id).get('Items')), 200


def current_time():
    now = datetime.datetime.now(tz=pytz.utc)
    tz = pytz.timezone("US/Pacific")
    date_format = "%Y-%m-%d %H:%M:%S"
    return now.astimezone(pytz.timezone('US/Pacific')).strftime(date_format)
