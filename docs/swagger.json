{
  "swagger": "2.0",
  "info": {
    "description": "This is an API to control Forum Posts and related comments.",
    "version": "0.0.1",
    "title": "Forum API"
  },
  "tags": [
    {
      "name": "Post"
    },
    {
      "name": "Comment"
    }
  ],
  "schemes": [
    "https",
    "http"
  ],
  "paths": {
    "/posts": {
      "post": {
        "tags": [
          "Post"
        ],
        "summary": "Add a new forum post",
        "operationId": "createPost",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "description": "Create a new post",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Post"
            }
          }
        ],
        "responses": {
          "201": {
            "description": "Post created",
            "schema": {
              "$ref": "#/definitions/PostResponse"
            }
          }
        }
      },
      "get": {
        "tags": [
          "Post"
        ],
        "summary": "Get all posts",
        "operationId": "getPosts",
        "produces": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "Success",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/PostResponse"
              }
            }
          }
        }
      }
    },
    "/posts/{post_id}": {
      "get": {
        "tags": [
          "Post"
        ],
        "description": "Get individual post by ID",
        "operationId": "getPost",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "post_id",
            "type": "string",
            "format": "uuid",
            "in": "path",
            "description": "ID of post that needs to be fetched",
            "required": true
          }
        ],
        "responses": {
          "200": {
            "description": "Success",
            "schema": {
              "$ref": "#/definitions/PostResponse"
            }
          },
          "404": {
            "description": "Post does not exist"
          }
        }
      },
      "patch": {
        "tags": [
          "Post"
        ],
        "description": "Update post",
        "operationId": "updatePost",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "post_id",
            "type": "string",
            "format": "uuid",
            "in": "path",
            "description": "ID of post that needs to be fetched",
            "required": true
          },
          {
            "in": "body",
            "name": "body",
            "description": "Create a new post",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Post"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Success",
            "schema": {
              "$ref": "#/definitions/PostResponse"
            }
          },
          "404": {
            "description": "Post does not exist"
          }
        }
      },
      "delete": {
        "tags": [
          "Post"
        ],
        "description": "Delete post by ID",
        "operationId": "deletePost",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "post_id",
            "type": "string",
            "format": "uuid",
            "in": "path",
            "description": "ID of post that needs to be fetched",
            "required": true
          }
        ],
        "responses": {
          "200": {
            "description": "Success"
          },
          "404": {
            "description": "Post does not exist"
          }
        }
      }
    },
    "/posts/{post_id}/comments": {
      "get": {
        "tags": [
          "Comment"
        ],
        "description": "Retrieves all comments for a given post ID",
        "operationId": "getComments",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "post_id",
            "type": "string",
            "format": "uuid",
            "in": "path",
            "description": "ID of parent post",
            "required": true
          }
        ],
        "responses": {
          "200": {
            "description": "Success",
            "schema": {
              "type": "array",
              "items": {
                "$ref": "#/definitions/CommentResponse"
              }
            }
          }
        }
      }
    },
    "/posts/{post_id}/comments/{comment_id}": {
      "get": {
        "tags": [
          "Comment"
        ],
        "description": "Get individual comment by ID",
        "operationId": "getComment",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "post_id",
            "type": "string",
            "format": "uuid",
            "in": "path",
            "description": "ID of parent post",
            "required": true
          },
          {
            "name": "comment_id",
            "type": "string",
            "format": "uuid",
            "in": "path",
            "description": "ID of comment to be retrieved",
            "required": true
          }
        ],
        "responses": {
          "200": {
            "description": "Success",
            "schema": {
              "$ref": "#/definitions/CommentResponse"
            }
          },
          "404": {
            "description": "Comment does not exist"
          }
        }
      },
      "patch": {
        "tags": [
          "Comment"
        ],
        "description": "Update comment",
        "operationId": "updateComment",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "post_id",
            "type": "string",
            "format": "uuid",
            "in": "path",
            "description": "ID of parent post",
            "required": true
          },
          {
            "name": "comment_id",
            "type": "string",
            "format": "uuid",
            "in": "path",
            "description": "ID of comment to be retrieved",
            "required": true
          },
          {
            "in": "body",
            "name": "body",
            "description": "Create a new post",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Comment"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Success",
            "schema": {
              "$ref": "#/definitions/CommentResponse"
            }
          },
          "404": {
            "description": "Comment does not exist"
          }
        }
      },
      "delete": {
        "tags": [
          "Post"
        ],
        "description": "Delete comment by ID",
        "operationId": "deleteComment",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "post_id",
            "type": "string",
            "format": "uuid",
            "in": "path",
            "description": "ID of parent post",
            "required": true
          },
          {
            "name": "comment_id",
            "type": "string",
            "format": "uuid",
            "in": "path",
            "description": "ID of comment to be retrieved",
            "required": true
          }
        ],
        "responses": {
          "200": {
            "description": "Success"
          },
          "404": {
            "description": "Comment does not exist"
          }
        }
      }
    }
  },
  "definitions": {
    "Post": {
      "type": "object",
      "properties": {
        "title": {
          "type": "string"
        },
        "body": {
          "type": "string"
        }
      }
    },
    "PostResponse": {
      "type": "object",
      "properties": {
        "id": {
          "type": "string",
          "format": "uuid"
        },
        "title": {
          "type": "string"
        },
        "body": {
          "type": "string"
        },
        "created_at": {
          "type": "string",
          "format": "date-time"
        },
        "updated_at": {
          "type": "string",
          "format": "date-time"
        }
      }
    },
    "Comment": {
      "type": "object",
      "properties": {
        "body": {
          "type": "string"
        }
      }
    },
    "CommentResponse": {
      "type": "object",
      "properties": {
        "id": {
          "type": "string",
          "format": "uuid"
        },
        "post_id": {
          "type": "string",
          "format": "uuid"
        },
        "body": {
          "type": "string"
        },
        "created_at": {
          "type": "string",
          "format": "date-time"
        },
        "updated_at": {
          "type": "string",
          "format": "date-time"
        }
      }
    }
  }
}