# api.py

import os
import boto3

from flask import Flask, request, render_template, jsonify
from controller import forumController

app = Flask(__name__)

POSTS_TABLE = os.environ['POST_TABLE']
client = boto3.client('dynamodb')


@app.route("/posts", methods=['POST', 'GET'])
def posts():
    # POST: Create Post
    if request.method == 'POST':
        return forumController.save_post()
    # GET: Post collection
    return forumController.get_posts()


@app.route("/posts/<string:post_id>", methods=['PATCH', 'GET', 'DELETE'])
def post(post_id):
    # PATCH: Update Post
    if request.method == 'PATCH':
        return forumController.update_post(post_id)
    elif request.method == 'DELETE':
        return forumController.delete_post(post_id)
    # GET: Get individual Post
    return forumController.get_post(post_id)


@app.route("/posts/<string:post_id>/comments", methods=['POST', 'GET'])
def comments(post_id):
    # POST: Create Comment
    if request.method == 'POST':
        return forumController.save_comment(post_id)
    # GET: Comment collection
    return forumController.get_comments(post_id)


@app.route("/posts/<string:post_id>/comments/<string:comment_id>", methods=['PATCH', 'GET', 'DELETE'])
def comment(post_id, comment_id):
    # PATCH: Update Comment
    if request.method == 'PATCH':
        return forumController.update_comment(post_id, comment_id)
    elif request.method == 'DELETE':
        return forumController.delete_comment(post_id, comment_id)
    # GET: Get individual Comment
    return forumController.get_comment(post_id, comment_id)


