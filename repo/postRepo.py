import os

import boto3

POST_TABLE = os.environ['POST_TABLE']
client = boto3.client('dynamodb')

dynamodb = boto3.resource('dynamodb', region_name="us-east-1")
table = dynamodb.Table(os.environ['POST_TABLE'])


def put_item(body, created_at, post_id, title, updated_at):
    return table.put_item(
        Item={
            'id': post_id,
            'title': title,
            'body': body,
            'created_at': created_at,
            'updated_at': updated_at
        },
        ConditionExpression='attribute_not_exists(title)'
    )


def get_post(post_id):
    return table.get_item(Key={'id': post_id})


def delete_post(post_id):
    return table.delete_item(Key={'id': post_id})


def update_post(body, post_id, title, updated_at):
    table.update_item(
        Key={'id': post_id},
        UpdateExpression="set title=:t, body=:b, updated_at=:u",
        ExpressionAttributeValues={
            ':t': title,
            ':b': body,
            ':u': updated_at
        },
        ReturnValues="UPDATED_NEW"
    )

    return get_post(post_id)


def get_posts_by_title(title):
    return table.query(
        Select='ALL_PROJECTED_ATTRIBUTES',
        IndexName='title-index',
        KeyConditionExpression='title = :title',
        ExpressionAttributeValues={":title": title}
    )


def scan_posts():
    return table.scan()
