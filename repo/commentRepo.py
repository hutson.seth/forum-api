import os

import boto3

COMMENT_TABLE = os.environ['COMMENT_TABLE']
client = boto3.client('dynamodb')

dynamodb = boto3.resource('dynamodb', region_name="us-east-1")
table = dynamodb.Table(os.environ['COMMENT_TABLE'])


def put_comment(body, comment_id, created_at, post_id, updated_at):
    return table.put_item(
        Item={
            'post_id': post_id,
            'id': comment_id,
            'body': body,
            'created_at': created_at,
            'updated_at': updated_at
        }
    )


def get_comment(comment_id):
    return table.get_item(
        Key={'id': comment_id},
    )


def delete_comment(comment_id):
    return table.delete_item(
        Key={'id': comment_id},
    )


def get_comments_by_post_id(post_id):
    return table.query(
        Select='ALL_PROJECTED_ATTRIBUTES',
        IndexName='post_id-index',
        KeyConditionExpression='post_id = :post_id',
        ExpressionAttributeValues={":post_id":  post_id}
    )


def update_comment(body, comment_id, updated_at):
    table.update_item(
        Key={'id': comment_id},
        UpdateExpression="set body=:b, updated_at=:u",
        ExpressionAttributeValues={
            ':b': body,
            ':u': updated_at
        },
        ReturnValues="UPDATED_NEW"
    )

    return get_comment(comment_id)
