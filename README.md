# Forum API

This REST API is used for storing, updating, reading, and deleting posts and comments to be used for a simple forum client.

## Getting Started

This API is already deployed to AWS Lambda so if you want to test the service simply download  the [postman](https://gitlab.com/hutson.seth/forum-api/-/blob/master/tests/ForumAPI.postman_collection.json) collection and follow the swagger documentation found [here](https://forum-api-bucket.s3.amazonaws.com/swagger.html). 

**Note:** This collection uses environment variables that are set automatically. To set the `post_id` variable you need to call the `POST /posts` endpoint first which will create a new post and store the id. Similarly, you will need to do this for the `comment_id` variable with the `POST /comments` endpoint but keep in mind that the comment object is dependent on a parent post object to exist.


## Development
There is currently no local development for this project. Simply follow the installation instructions at [Serverless](https://www.serverless.com/framework/docs/getting-started) and run the command `sls deploy`.
